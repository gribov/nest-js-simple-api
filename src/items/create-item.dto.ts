import { IsNotEmpty, IsEmpty, IsIn } from 'class-validator';

export class CreateItemDto {
  @IsEmpty()
  id: number;

  @IsNotEmpty()
  @IsIn(['vegetable', 'fruit'])
  type: string;

  @IsNotEmpty()
  category: number;
}
