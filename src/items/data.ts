export default [
  {
    id: 1,
    type: 'fruit',
    category: 6,
  },
  {
    id: 2,
    type: 'fruit',
    category: 6,
  },
  {
    id: 3,
    type: 'fruit',
    category: 6,
  },
  {
    id: 1,
    type: 'vegetable',
    category: 5,
  },
  {
    id: 4,
    type: 'vegetable',
    category: 5,
  },
  {
    id: 5,
    type: 'fruit',
    category: 6,
  }
];
