import { Item } from './item.interface';

export class ItemEntity implements Item {
  id: number;

  type: string;

  category: number;

  constructor(partial: Partial<ItemEntity>) {
    Object.assign(this, partial);
  }
}
