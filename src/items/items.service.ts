import { Injectable } from '@nestjs/common';
import { Item } from './item.interface';
import { ItemEntity } from './item.entity';

import data from './data';

@Injectable()
export class ItemsService {
  private readonly items: Item[] = data;
  private count = 6;

  create(item: Item): Item {
    const entity = new ItemEntity({
      id: this.count,
      type: item.type,
      category: item.category,
    });
    this.count = this.count + 1;
    this.items.push(entity);
    return entity;
  }

  findAll(): Item[] {
    return this.items;
  }
}
