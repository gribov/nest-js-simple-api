import {
  ClassSerializerInterceptor,
  Controller,
  Get,
  UseInterceptors,
} from '@nestjs/common';

import { AppService } from './app.service';
import { UserEntity } from './entities/user.entity';

@Controller()
@UseInterceptors(ClassSerializerInterceptor)
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): UserEntity {
    return new UserEntity({
      id: 1,
      firstName: 'Test',
      lastName: 'Bar',
      password: 'password',
      role: 'admin',
    });
  }
}
